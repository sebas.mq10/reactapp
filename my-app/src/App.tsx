import "./App.css";
import NavBar from "./components/NavBar";
import { Jugadores } from "./components/Jugadores";
import { Controles } from "./components/Controles";
import {Boton } from "./components/Boton"
import {Counter} from "./components/Counter"
import { Resultados } from "./components/Resultados";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <NavBar />
      </header>
      <Jugadores
        title="Jugadores"
        players={{
          jugador1: "Cami",
          jugador2: "Mati",
          jugador3: "Seba",
          jugador4: "Leo",
          jugador5: "Lean",
        }}
      />
      <Controles
        title="Controles"
        josticks={{
          control1: "Gamecube",
          control2: "Bueno",
          control3: "Rojo y verde",
          control4: "Rojo",
          control5: "Malo",
        }}
      />
      <Boton />
      <Counter/>
      <Resultados />
    </div>
  );
};

export default App;
