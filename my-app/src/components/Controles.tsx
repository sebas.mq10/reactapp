export function Controles(props: any) {
  return (
    <div className="contenedor-controles">
      <h1>{props.title}</h1>
      <ul>
        <li>{props.josticks.control1}</li>
        <li>{props.josticks.control2}</li>
        <li>{props.josticks.control3}</li>
        <li>{props.josticks.control4}</li>
        <li>{props.josticks.control5}</li>
      </ul>
    </div>
  );
}
