export function Jugadores(props: any) {
  return (
    <div className="contenedor-jugadores">
      <h1>{props.title}</h1>
      <ul>
        <li>{props.players.jugador1}</li>
        <li>{props.players.jugador2}</li>
        <li>{props.players.jugador3}</li>
        <li>{props.players.jugador4}</li>
        <li>{props.players.jugador5}</li>
      </ul>
    </div>
  );
}
